package onhome

import onhome.environment.JenkinsJobs
import onhome.environment.cicd.FunctionsCD
import onhome.environment.cicd.FunctionsCI
import onhome.tools.docker.Docker
import onhome.wrappers.Wrapper

class Functions implements Serializable {
    def all
    private static Functions functions

    public Wrapper wrapper
    public JenkinsJobs job
    public Docker docker
    public FunctionsCI ci
    public FunctionsCD cd


    private Functions(def all){
        this.all = all
        wrapper = new Wrapper(all)
        job = new JenkinsJobs(all)

        docker = new Docker(this)
        ci = new FunctionsCI(this)
    }
    static Functions getInstance(def all){
        if(functions != null)
            return functions
        functions = new Functions(all)
        return functions
    }
    def envF(String text){
        wrapper.envCommand(text)
    }
    def fileExistsF(String text){
        wrapper.fileExistsCommand(text)
    }
}
