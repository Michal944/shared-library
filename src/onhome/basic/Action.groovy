package onhome.basic

interface Action {
    def description(Map input)
    def input(Map input)
    def perform(Map input)
}