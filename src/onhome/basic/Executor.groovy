package onhome.basic


import onhome.exceptions.FileNotExists
import onhome.exceptions.ImageNotExists

class Executor {
    static def perform(Action action, Map input, Closure closure){
        try{
            action.description(input)
            action.input(input)
            action.perform(input)
        } catch(FileNotExists e) {
            input.each {closure("${it.key}:  ${it.value}")}
            closure(e)
        } catch (ImageNotExists e){
            input.each {closure("${it.key}:  ${it.value}")}
            closure(e)
        }

    }
}
