package onhome.environment

class Env {
    enum REPO {
        defaultRegistry("registry.gitlab.com/michal944")
        private final String url

        REPO(String url){this.url = url}
        String get(){
            return url
        }
        String getUrl(){
            return "https://" + url
        }
    }
}
