package onhome.environment

import onhome.wrappers.Wrapper

class JenkinsJobs {
    def all
    JenkinsJobs(def all){
        this.all = all
    }

    def isReleaseBranch(){
        String envBranchName = all.env.BRANCH_NAME
        if(envBranchName.toLowerCase().contains("release"))
            return true
        return false
    }
    def jobName(){
        String envJobName = all.env.JOB_NAME
        String[] appName = envJobName.split("/")
        if(isReleaseBranch())
            return appName[0]
        else
            return appName[0] + "tests"
    }
    def buildNumber(){
        return all.env.BUILD_NUMBER
    }
    def imageName(){
        return jobName() + ":" + buildNumber()
    }
    def containerName(){
        String envJobName = all.env.JOB_NAME
        String[] appName = envJobName.split("/")
        return appName[0] + "-" + buildNumber()
    }
}
