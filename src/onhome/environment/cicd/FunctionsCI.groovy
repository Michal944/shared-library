package onhome.environment.cicd

import onhome.Functions

class FunctionsCI {
    private Functions all
    FunctionsCI(Functions all){
        this.all = all
    }
    boolean isBuildFromRelease(){
        String branchName = all.envF("BRANCH_NAME")
        return branchName.toLowerCase().contains("release")
    }
}
