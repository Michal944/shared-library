package onhome.tools.docker

import onhome.Functions
import onhome.tools.docker.Docker
import onhome.tools.docker.basic.Cleaner

import javax.print.Doc

class Container implements Cleaner {
    private String containerName
    Docker all
    protected Container(Map input, Docker all){
        this.containerName = input.name
        this.all = all
    }
    def exec(Map input=[:]){
        input.name = containerName
        all.exec(input)
    }
    def clean(){
        Map input = [:]
        input.name = containerName
        all.stop(input)
        all.rm(input)
    }
    def copyTo(String src, String target){
        Map input = [:]
        input.source = src
        input.target = containerName + ":" + target
        all.copy(input)
    }
    def copyFrom(String src, String target){
        Map input = [:]
        input.source = containerName + ":" + src
        input.target = target
        all.copy(input)
    }
}
