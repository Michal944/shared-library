package onhome.tools.docker

import onhome.Functions
import onhome.basic.Executor

class Docker {
    Functions all
    private DockerBuild builder
    private DockerPush pusher
    private DockerExec exec
    private DockerRun runner
    private DockerStop stop
    private DockerRm rm
    private DockerImageRm imageRm
    private DockerCopy copy
    def echo = { all.wrapper.echoCommand(it.toString())}

    Docker(Functions all){
        this.all = all
        builder = new DockerBuild(all)
        pusher  = new DockerPush(all)
        exec    = new DockerExec(all)
        runner  = new DockerRun(all)
        stop    = new DockerStop(all)
        rm      = new DockerRm(all)
        imageRm = new DockerImageRm(all)
        copy    = new DockerCopy(all)
    }

    def build(Map input){
        Executor.perform(builder, input, echo)
        return new Image(input, this)
    }
    def push(Map input){
        Executor.perform(pusher, input, echo)
    }
    def run(Map input){
        Executor.perform(runner, input, echo)
        return new Container(input, this)
    }
    def exec(Map input){
        Executor.perform(exec, input, echo)
    }
    def stop(Map input){
        Executor.perform(stop, input, echo)
    }
    def rm(Map input){
        Executor.perform(rm, input, echo)
    }
    def imageRm(Map input){
        Executor.perform(imageRm, input, echo)
    }
    def copy(Map input){
        Executor.perform(copy, input, echo)
    }
}
