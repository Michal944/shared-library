package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action
import onhome.environment.Env
import onhome.exceptions.FileNotExists

class DockerBuild implements Action{
    private Functions all

    protected DockerBuild(Functions all){
        this.all = all
    }
    @Override
    def description(Map input){
        input.description = "Start docker build -> Docker's API trying to build the image from Dockerfile"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("repository", Env.REPO.defaultRegistry.get())
        input.putIfAbsent("tag", input.repository + "/" + all.job.imageName())
        input.putIfAbsent("dockerfile", "Dockerfile")
    }

    @Override
    def perform(Map input){
        //if( ! all.fileExistsF("${input.dockerfile}")) throw new FileNotExists()
        all.wrapper.shCommand(script: "docker build -f ${input.dockerfile} --tag ${input.tag} .")
    }


}
