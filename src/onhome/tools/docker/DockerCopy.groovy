package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action

class DockerCopy implements Action {
    private Functions all
    protected DockerCopy(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "docker copy -> Docker's API trying to COPY the files"
    }

    @Override
    def input(Map input){
       // input.putIfAbsent("version", all.envF("BUILD_NUMBER") )
       // input.putIfAbsent("name", all.envF("JOB_NAME") + all.envF("BRANCH_NAME").toString().substring(0, 1).toUpperCase() +
        //        all.envF("BRANCH_NAME").toString().substring(1) + ":" + input.version)
    }

    @Override
    def perform(Map input){
        input.putIfAbsent("returnStdout", false)
        all.wrapper.shCommand(script: "docker cp ${input.source} ${input.target}", returnStdout: input.returnStdout as boolean)
    }
}
