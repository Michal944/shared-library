package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action

class DockerExec implements Action {
    private Functions all
    protected DockerExec(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "docker exec -> Docker's API trying to EXECUTE the command inside container"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("name", all.job.containerName())
    }


    @Override
    def perform(Map input){
        input.putIfAbsent("returnStdout", false)
        if(input.containsKey("command"))
            all.wrapper.shCommand(script: "docker exec ${input.name} ${input.command}", returnStdout: input.returnStdout as boolean)
    }
}
