package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action

class DockerImageRm implements Action{
    private Functions all
    protected DockerImageRm(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "docker stop -> Docker's API trying to STOP container"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("image",  input.repository + "/" + all.job.imageName())
    }

    @Override
    def perform(Map input){
        String isExistsImage = all.wrapper.shCommand(script: "docker image ls --filter=reference=\'${input.image}\' | grep -v \"REPOSITORY\"", returnStdout: true)
        if(! isExistsImage.isEmpty() )
            all.wrapper.shCommand(script: "docker image rm  ${input.image}")
    }
}
