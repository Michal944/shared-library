package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action
import onhome.environment.Env
import onhome.exceptions.ImageNotExists

class DockerPull implements Action {
    private Functions all
    protected DockerPull(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "Start docker pull image from repository -> Docker's API trying to pull the image from repository"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("repository", Env.REPO.defaultRegistry.get())
        input.putIfAbsent("tag", input.repository + "/" + all.job.imageName())
    }

    @Override
    def perform(Map input){
        all.wrapper.shCommand(script:"docker pull ${input.tag}")
        String image = all.wrapper.shCommand(script: "docker image ls ${input.tag} | grep -vi \"repository\" | awk {\'print \$1\'}", returnStdout:  true)
        if( image.isEmpty() ) throw new ImageNotExists()
    }
}
