package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action
import onhome.environment.Env

class DockerPush implements Action {
    private Functions all
    protected DockerPush(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "Start docker push to repository -> Docker's API trying to push the image to repository"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("repository", Env.REPO.defaultRegistry.get())
        input.putIfAbsent("tag", input.repository + "/" + all.job.imageName())
        input.putIfAbsent("dockerfile", "Dockerfile")    }

    @Override
    def perform(Map input){
        String image = all.wrapper.shCommand(script: "docker image ls ${input.tag} | grep -vi \"repository\" | awk {\'print \$1\'}",returnStdout: true)
        if( ! (image.isEmpty() || image.isBlank())  )
            all.wrapper.shCommand(script: "docker push ${input.tag}")
    }
}
