package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action

class DockerRm implements Action{
    private Functions all
    protected DockerRm(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "docker remove -> Docker's API trying to REMOVE container"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("version", all.envF("BUILD_NUMBER") )
        input.putIfAbsent("name", all.job.containerName())
    }

    @Override
    def perform(Map input){
        String isExistsContainer =all.wrapper.shCommand(script:"docker ps -a -f \"name=${input.name}\" | grep -v \"CONTAINER\" | awk {'print \$1'}", returnStdout: true)
        if(! isExistsContainer.isEmpty() )
            all.wrapper.shCommand(script: "docker rm ${input.name}")
    }
}
