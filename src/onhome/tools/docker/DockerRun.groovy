package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action
import onhome.environment.Env

class DockerRun implements Action {
    private Functions all
    protected DockerRun(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "Start docker run to repository -> Docker's API trying to RUN the image to repository"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("repository", Env.REPO.defaultRegistry.get())
        input.putIfAbsent("image", input.repository + "/" + all.job.imageName())
        input.putIfAbsent("name", all.job.containerName())
        input.putIfAbsent("options", "--rm -d ")
        input.putIfAbsent("volume", "")
    }

    @Override
    def perform(Map input){
        String image = all.wrapper.shCommand(script: "docker image ls ${input.image} | grep -vi \"repository\" | awk {\'print \$1\'}", returnStdout: true)
        if( ! (image.isEmpty())  )
        {
            if(input.volume.toString().isEmpty())
            {
                all.wrapper.shCommand(script: "docker run --name ${input.name} ${input.options} ${input.image} .")
            }
            else
            {
                all.wrapper.shCommand(script: "docker run --name ${input.name} ${input.options} -v ${input.volume} ${input.image} .")
            }
        }
    }
}
