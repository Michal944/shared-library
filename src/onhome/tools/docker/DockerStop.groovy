package onhome.tools.docker

import onhome.Functions
import onhome.basic.Action

class DockerStop implements Action {
    private Functions all
    protected DockerStop(Functions all){
        this.all = all
    }

    @Override
    def description(Map input){
        input.description = "docker stop -> Docker's API trying to STOP container"
    }

    @Override
    def input(Map input){
        input.putIfAbsent("name", all.job.containerName())
    }

    @Override
    def perform(Map input){
        String isExistsContainer = all.wrapper.shCommand(script: "docker ps -f \"name=${input.name}\" | grep -v \"CONTAINER\" | awk {'print \$1'}", returnStdout: true)
        if(! isExistsContainer.isEmpty() )
            all.wrapper.shCommand(script: "docker stop ${input.name}")
    }
}
