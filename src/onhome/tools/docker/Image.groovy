package onhome.tools.docker

import onhome.tools.docker.basic.Cleaner

class Image implements Cleaner {
    private String imageName
    boolean isBuilt = false
    Docker all
    protected Image(Map input, Docker all){
        this.imageName = input.tag
        this.all = all
    }
    def run(Map input = [:]){
        input.image = imageName
        return all.run(input)
    }

    def clean(Map input = [:]){
        input.image = imageName
        all.imageRm(input)
    }

    def push(Map input = [:]){
        input.tag = imageName
        all.push(input)
    }

}
