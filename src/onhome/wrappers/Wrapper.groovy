package onhome.wrappers

class Wrapper{
    Map VARIABLE = [:]
    def all
    Wrapper(def all){
        this.all = all
        VARIABLE["JOB_NAME"] = this.all.env.JOB_NAME
        VARIABLE["BUILD_NUMBER"] = this.all.env.BUILD_NUMBER
        VARIABLE["BRANCH_NAME"] = this.all.env.BRANCH_NAME
    }
    def shCommand(Map input){
        input.putIfAbsent("returnStdout", false)
        if(input.returnStdout as boolean)
            return all.sh(script: "${input.script}", returnStdout: true)
        all.sh(script: "${input.script}")
    }
    def echoCommand(def text){
        all.sh("echo \"${text}\"")
    }
    def envCommand(def text){
        return VARIABLE.get(text)
    }
    def fileExistsCommand(def text){
        all.fileExists(text)
    }
    def readFileCommand(def file){
        all.readFile(file: file)
    }
}
